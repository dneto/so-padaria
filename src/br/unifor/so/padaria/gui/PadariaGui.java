package br.unifor.so.padaria.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.LayoutManager;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.ScrollPaneLayout;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JScrollPane;
import java.awt.GridBagConstraints;
import javax.swing.JTextArea;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JMenuBar;
import javax.swing.JButton;

public class PadariaGui extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PadariaGui frame = new PadariaGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PadariaGui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setResizable(false);
		setSize(500, 500);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0,
				Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 1.0, 1.0, 1.0,
				Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.gridwidth = 8;
		gbc_panel.insets = new Insets(0, 0, 5, 5);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		contentPane.add(panel, gbc_panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));

		JLabel lblNewLabel = new JLabel("Vendedores");
		lblNewLabel.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		panel.add(lblNewLabel);

		JPanel panel_1 = new JPanel();
		panel.add(panel_1);

		JPanel panel_2 = new JPanel();
		panel.add(panel_2);

		JPanel panel_3 = new JPanel();
		panel.add(panel_3);

		JPanel panel_4 = new JPanel();
		panel.add(panel_4);

		JPanel panel_5 = new JPanel();
		panel.add(panel_5);

		JPanel panel_6 = new JPanel();
		GridBagConstraints gbc_panel_6 = new GridBagConstraints();
		gbc_panel_6.gridheight = 4;
		gbc_panel_6.gridwidth = 8;
		gbc_panel_6.insets = new Insets(0, 0, 5, 5);
		gbc_panel_6.fill = GridBagConstraints.BOTH;
		gbc_panel_6.gridx = 8;
		gbc_panel_6.gridy = 0;
		contentPane.add(panel_6, gbc_panel_6);
		GridBagLayout gbl_panel_6 = new GridBagLayout();
		
		panel_6.setLayout(gbl_panel_6);

		JLabel lblNewLabel_1 = new JLabel("Clientes");
		lblNewLabel_1.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_1.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel_1.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 0;
		panel_6.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		JButton btnAdicionarCliente = new JButton("Adicionar Cliente");
		GridBagConstraints gbc_btnAdicionarCliente = new GridBagConstraints();
		gbc_btnAdicionarCliente.anchor = GridBagConstraints.NORTHWEST;
		gbc_btnAdicionarCliente.fill = GridBagConstraints.BOTH;
		gbc_btnAdicionarCliente.insets = new Insets(0, 0, 5, 0);
		gbc_btnAdicionarCliente.gridx = 0;
		gbc_btnAdicionarCliente.gridy = 1;
		panel_6.add(btnAdicionarCliente, gbc_btnAdicionarCliente);

		JScrollPane scrollPane_1 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.gridx = 0;
		gbc_scrollPane_1.gridy = 2;
		gbc_scrollPane_1.weightx = 2.0;
		gbc_scrollPane_1.weighty = 2.0;
		gbc_scrollPane_1.gridheight = 1;
		gbc_scrollPane_1.gridwidth = 1;

		panel_6.add(scrollPane_1, gbc_scrollPane_1);

		JPanel logPanel = new JPanel();
		GridBagConstraints gbc_logPanel = new GridBagConstraints();
		gbc_logPanel.gridheight = 3;
		gbc_logPanel.gridwidth = 8;
		gbc_logPanel.insets = new Insets(0, 0, 0, 5);
		gbc_logPanel.fill = GridBagConstraints.BOTH;
		gbc_logPanel.gridx = 0;
		gbc_logPanel.gridy = 1;
		contentPane.add(logPanel, gbc_logPanel);
		GridBagLayout gbl_logPanel = new GridBagLayout();

		logPanel.setLayout(gbl_logPanel);

		JLabel lblLog = new JLabel("Log");
		lblLog.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		lblLog.setAlignmentX(LEFT_ALIGNMENT);
		GridBagConstraints gbc_lblLog = new GridBagConstraints();
		gbc_lblLog.anchor = GridBagConstraints.WEST;
		gbc_lblLog.gridx = 0;
		gbc_lblLog.gridy = 0;
		logPanel.add(lblLog, gbc_lblLog);

		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 1;
		gbc_scrollPane.weightx = 4;
		gbc_scrollPane.weighty = 4;
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		logPanel.add(scrollPane, gbc_scrollPane);
		scrollPane.setLayout(new ScrollPaneLayout());

		JTextArea textArea = new JTextArea();
		textArea.setRows(1);
		textArea.setFont(new Font("Courier New", Font.PLAIN, 10));
		textArea.setEditable(false);
		scrollPane.setViewportView(textArea);
	}

}
