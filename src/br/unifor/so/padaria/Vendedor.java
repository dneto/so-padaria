package br.unifor.so.padaria;

import static br.unifor.so.padaria.semaphore.SemaphoreUtils.down;
import static br.unifor.so.padaria.semaphore.SemaphoreUtils.up;

public class Vendedor extends Thread {

	private final Padaria padaria;

	private String nome;
	private Cliente cliente;
	private long tempoAtendimento;

	public Vendedor(String nome, Padaria padaria) {
		super(nome);
		this.padaria = padaria;
	}

	@Override
	public void run() {

		while (true) {
			System.out.println("Vendedor " + getName() + " Dormiu ");
			down(padaria.getMutexCliente());
			System.out.println("Vendedor " + getName() + " Acordou ");

			down(padaria.getMutex());
			padaria.decrementarClientesEmEspera();
			up(padaria.getMutex());

			padaria.pushVendedor(this);

			up(padaria.getMutexVendedor());
			down(padaria.getMutexAtendimentoCliente());
			cliente = padaria.popCliente();
			cliente.setVendedor(this);
			this.tempoAtendimento = cliente.getTempoAtendimento();
			this.atendimento();
			System.out.println(getName() + "Liberado");
		}

	}

	public void atendimento() {

		long init = System.currentTimeMillis();
		long milis;
		long dif = 0;
		long difAux = 0;

		do {
			milis = System.currentTimeMillis();
			dif = (milis - init) / 1000;

			if (dif != difAux) {
				difAux = dif;
			}

		} while (dif < tempoAtendimento);

		while (cliente.isAlive()) {
		}
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}
