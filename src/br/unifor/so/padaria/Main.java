package br.unifor.so.padaria;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Padaria p = new Padaria(5);

		Scanner s = new Scanner(System.in);

		String str;
		do {
			str = s.nextLine();
			int time = Integer.parseInt(str);
			p.adicionarCliente(time);

		} while (!str.equals("exit"));

		s.close();

	}

}
