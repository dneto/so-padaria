package br.unifor.so.padaria.events;

public interface Event {
	public void execute();
}
