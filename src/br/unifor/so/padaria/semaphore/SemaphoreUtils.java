package br.unifor.so.padaria.semaphore;

import java.util.concurrent.Semaphore;


public class SemaphoreUtils {

	public static void down(Semaphore s) {
		try {
			s.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void up(Semaphore s) {
		s.release();
	}

}
