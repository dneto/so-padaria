package br.unifor.so.padaria;

import static br.unifor.so.padaria.semaphore.SemaphoreUtils.*;

public class Cliente extends Thread {
	private final Padaria padaria;

	private int senha;
	private String nome;
	private long tempoAtendimento;
	private Vendedor vendedor;

	public Cliente(String nome, long tempoAtendimento, Padaria padaria) {
		super(nome);
		this.tempoAtendimento = tempoAtendimento;
		this.padaria = padaria;
	}

	@Override
	public void run() {

		down(padaria.getMutex());

		System.out.println("Cliente " + Thread.currentThread().getName()
				+ " Chegou");
		padaria.incrementarClientesEmEspera();
		up(padaria.getMutexCliente());

		up(padaria.getMutex());

		System.out.println("Cliente " + Thread.currentThread().getName()
				+ " Dormiu");

		padaria.pushCliente(this);
		down(padaria.getMutexVendedor());

		System.out.println("Cliente " + Thread.currentThread().getName()
				+ " Acordou");
		up(padaria.getMutexAtendimentoCliente());

		atendimento();

	}

	public void atendimento() {

		long init = System.currentTimeMillis();
		long milis;
		long dif = 0;
		long difAux = 0;

		do {
			milis = System.currentTimeMillis();
			dif = (milis - init) / 1000;

			if (dif != difAux) {
				difAux = dif;
				System.out.println("[C] Vendedor:" + vendedor.getName()
						+ " Cliente" + this.getName() + " Tempo: " + difAux
						+ "/" + tempoAtendimento);
			}

		} while (dif < tempoAtendimento);

	}

	public int getSenha() {
		return senha;
	}

	public void setSenha(int senha) {
		this.senha = senha;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getTempoAtendimento() {
		return tempoAtendimento;
	}

	public void setTempoAtendimento(long tempoAtendimento) {
		this.tempoAtendimento = tempoAtendimento;
	}

	public Vendedor getVendedor() {
		return vendedor;
	}

	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}

}
