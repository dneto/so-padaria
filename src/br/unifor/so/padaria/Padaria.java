package br.unifor.so.padaria;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Padaria {
	
	private int numVendedores;
	private int numClientes;
	private int clientesEmEspera;
	private int codGen = 1;

	private final Semaphore mutexVendedor = new Semaphore(0);
	private final Semaphore mutexCliente = new Semaphore(0, true);
	private final Semaphore mutex = new Semaphore(1);
	private final Semaphore mutexAtendimentoCliente = new Semaphore(0, true);
	private final Semaphore mutexAtendimentoVendedor = new Semaphore(0, true);

	private final List<Cliente> filaClienteDisp = new ArrayList<Cliente>();
	private final List<Vendedor> filaVendedorDisp = new ArrayList<Vendedor>();

	public Padaria() {

	}

	public Padaria(int numVendedores) {
		this.numVendedores = numVendedores;

		for (int i = 1; i <= numVendedores; i++) {
			Vendedor v = new Vendedor("Vendedor-" + String.valueOf(i), this);
			v.start();
			System.out.println("Vendedor-" + i + " Criado");
		}
	}

	public void adicionarCliente(int time) {
		Cliente c = new Cliente(String.valueOf(codGen), time, this);
		c.start();
		codGen++;
		System.out.println("Cliente criado!");
	}

	public void incrementarVendedor() {
		numVendedores++;
	}

	public void decrementarVendedor() {
		numVendedores--;
	}

	public void incrementarCliente() {
		numClientes++;
	}

	public void decrementarCliente() {
		numClientes--;
	}

	public int getNumVendedores() {
		return numVendedores;
	}

	public void setNumVendedores(int numVendedores) {
		this.numVendedores = numVendedores;
	}

	public int getNumClientes() {
		return numClientes;
	}

	public void setNumClientes(int numClientes) {
		this.numClientes = numClientes;
	}

	public int getClientesEmEspera() {
		return clientesEmEspera;
	}

	public int incrementarClientesEmEspera() {
		return clientesEmEspera++;
	}

	public int decrementarClientesEmEspera() {
		return clientesEmEspera--;
	}

	public Semaphore getMutexVendedor() {
		return mutexVendedor;
	}

	public Semaphore getMutexCliente() {
		return mutexCliente;
	}

	public Semaphore getMutex() {
		return mutex;
	}

	public Semaphore getMutexAtendimentoCliente() {
		return mutexAtendimentoCliente;
	}

	public Semaphore getMutexAtendimentoVendedor() {
		return mutexAtendimentoVendedor;
	}

	public Cliente popCliente() {
		if (!filaClienteDisp.isEmpty()) {
			return filaClienteDisp.remove(0);
		}
		return null;
	}

	public void pushCliente(Cliente c) {
		filaClienteDisp.add(c);
	}

	public Vendedor popVendedor() {
		if (!filaVendedorDisp.isEmpty()) {
			return filaVendedorDisp.remove(0);
		}
		return null;
	}

	public void pushVendedor(Vendedor c) {
		filaVendedorDisp.add(c);
	}

}
